package models;

import java.util.ArrayList;

public interface dbPersistencia {
    public void insertar(Object obj) throws Exception;
    public void actualizar(Object obj) throws Exception;
    public void habilitar(Object obj) throws Exception;
    public void deshabilitar(Object obj) throws Exception;
    
    public boolean isExiste(String codigo, String criterio) throws Exception;
    public ArrayList listar() throws Exception;
    public ArrayList listar(String criterio) throws Exception;
    public Object buscar(int id) throws Exception;
    public Object buscar(String codigo, String criterio) throws Exception;
}
