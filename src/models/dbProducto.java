package models;

import java.sql.SQLException;
import java.util.ArrayList;

public class dbProducto extends DBManejador implements dbPersistencia{

    @Override
    public void insertar(Object obj) throws Exception {
        Productos pro = new Productos();
        pro = (Productos)obj;
        String consulta = "";
        consulta = "INSERT INTO productos(codigo, nombre, fecha, precio, status) VALUES (?, ?, ?, ? ,?);";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // Asignar los valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPrecio());
                this.sqlConsulta.setInt(5, pro.getStatus());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch(SQLException e){
                System.err.println("Surgio un error al insertar: " + e.getMessage());
            }
        }
    }

    @Override
    public void actualizar(Object obj) throws Exception {
        Productos pro = new Productos();
        pro = (Productos)obj;
        String consulta = "";
        consulta = "UPDATE productos SET nombre = ?, fecha = ?, precio = ? WHERE codigo = ?;";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // Asignar los valores a la consulta
                this.sqlConsulta.setString(1, pro.getNombre());
                this.sqlConsulta.setString(2, pro.getFecha());
                this.sqlConsulta.setFloat(3, pro.getPrecio());
                this.sqlConsulta.setString(4, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch(SQLException e){
                System.err.println("Surgio un error al actualizar: " + e.getMessage());
            }
        }
    }

    @Override
    public void habilitar(Object obj) throws Exception {
        Productos pro = new Productos();
        pro = (Productos)obj;
        String consulta = "";
        consulta = "UPDATE productos SET status = 0 WHERE codigo = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // Asignar los valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch(SQLException e){
                System.err.println("Surgio un error al habilitar: " + e.getMessage());
            }
        }
    }

    @Override
    public void deshabilitar(Object obj) throws Exception {
        Productos pro = new Productos();
        pro = (Productos)obj;
        String consulta = "";
        consulta = "UPDATE productos SET status = -1 WHERE codigo = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // Asignar los valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch(SQLException e){
                System.err.println("Surgio un error al habilitar: " + e.getMessage());
            }
        }
    }

    @Override
    public boolean isExiste(String codigo, String criterio) throws Exception {
        if(this.conectar()){
            String consulta = "SELECT * FROM productos WHERE codigo = ? and status = ?;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.sqlConsulta.setString(2, criterio);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                return true;
            }
        }
        this.desconectar();
        return false;
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        if(this.conectar()){
            String consulta = "SELECT * FROM productos ORDER BY codigo;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            while(this.registros.next()){
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        return lista;
    }

    @Override
    public ArrayList listar(String criterio) throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        if(this.conectar()){
            String consulta = "SELECT * FROM productos WHERE status = ?;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, criterio);
            this.registros = this.sqlConsulta.executeQuery();
            while(this.registros.next()){
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        return lista;
    }

    @Override
    public Object buscar(int id) throws Exception {
        Productos pro = new Productos();
        if(this.conectar()){
            String consulta = "SELECT * FROM productos WHERE idProducto = ? and status = 0;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setInt(1, id);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                pro.setIdProducto(this.registros.getInt("IdProducto"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
            }
        }
        this.desconectar();
        return pro;
    }

    @Override
    public Object buscar(String codigo, String criterio) throws Exception {
        Productos pro = null;
        if(this.conectar()){
            String consulta = "SELECT * FROM productos WHERE codigo = ? and status = ?;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.sqlConsulta.setString(2, criterio);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("IdProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
            }
        }
        this.desconectar();
        return pro;
    }
}
