package models;

import java.sql.Date;

public class Productos {
    private int idProducto;
    private String codigo;
    private String nombre;
    private float precio;
    private String fecha;
    private int status;

    public Productos(){
        this.idProducto = 0;
        this.codigo = "";
        this.nombre = "";
        this.precio = 0.0f;
        this.fecha = null;
        this.status = 0;
    }
    
    public Productos(int idProducto, String codigo, String nombre, float precio, String fecha, int status) {
        this.idProducto = idProducto;
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
        this.fecha = fecha;
        this.status = status;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Productos{" + "idProducto=" + idProducto + ", codigo=" + codigo + ", nombre=" + nombre + ", precio=" + precio + ", fecha=" + fecha + ", status=" + status + '}' + '\n';
    }
}
