package controllers;

import javax.swing.JOptionPane;
import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import models.dbProducto;
import models.Productos;
import views.dlgManejoDatos;

public class Controlador implements ActionListener{
    private Productos pro;
    private dbProducto db;
    private dlgManejoDatos vista;
    private static Boolean act = false;
    
    public Controlador(Productos pro, dbProducto db, dlgManejoDatos vista){
        this.pro = pro;
        this.db = db;
        this.vista = vista;
        // Escuchar la vista
        this.vista.btnNuevo.addActionListener(this);
        this.vista.btnGuardar.addActionListener(this);
        this.vista.btnMostrar.addActionListener(this);
        this.vista.btnBuscar.addActionListener(this);
        this.vista.btnLimpiar.addActionListener(this);
        this.vista.btnCancelar.addActionListener(this);
        this.vista.btnCerrar.addActionListener(this);
        this.vista.btnDeshabilitar.addActionListener(this);
        this.vista.btnBuscarInactivo.addActionListener(this);
        this.vista.btnHabilitar.addActionListener(this);
    }
    
    public static void main(String[] args){
        Productos pro = new Productos();
        dbProducto db = new dbProducto();
        dlgManejoDatos vista = new dlgManejoDatos(new JFrame(), true);
        Controlador controlador = new Controlador(pro, db, vista);
        controlador.iniciarVista();
    }
    
    public void iniciarVista(){
        this.vista.setSize(570, 500);
        this.vista.setTitle("Manejo Datos");
        this.vista.setLocationRelativeTo(null);
        this.mostrarRegistros("0", this.vista.jtTabla);
        this.mostrarRegistros("-1", this.vista.jtTablaInactivos);
        this.vista.setVisible(true);
    }
    
    private void habilitarComponentes(Boolean bool){
        this.vista.btnGuardar.setEnabled(bool);
        this.vista.btnMostrar.setEnabled(bool);
        this.vista.btnLimpiar.setEnabled(bool);
        this.vista.btnCancelar.setEnabled(bool);
        this.vista.txtNombre.setEnabled(bool);
        this.vista.txtPrecio.setEnabled(bool);
        this.vista.jCFecha.setEnabled(bool);
        this.vista.jcbStatus.setEnabled(bool);
    }
    
    private void limpiar(){
        this.vista.txtCodigo.setText("");
        this.vista.txtNombre.setText("");
        this.vista.txtPrecio.setText("");
        this.vista.jcbStatus.setSelectedIndex(0);
    }
    
    private Boolean isVacio(){
        return this.vista.txtCodigo.getText().equals("")||
               this.vista.txtNombre.getText().equals("")||
               this.vista.txtPrecio.getText().equals("");
    }
    
    private Boolean isValido(){
        try{
            if(Float.parseFloat(this.vista.txtPrecio.getText()) < 0){
                JOptionPane.showMessageDialog(this.vista, "No se puede capturar un precio negativo", this.vista.getTitle(), JOptionPane.ERROR_MESSAGE);
                return false;
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(this.vista, "Ocurrio un error: " + e.getMessage(), this.vista.getTitle(), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
    
    private void mostrarRegistros(String criterio, JTable tabla){
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Productos> lista = new ArrayList<>();
        try{
            lista = this.db.listar(criterio);
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(this.vista, "Ocurrio un error al listar: " + ex.getMessage(), this.vista.getTitle(), JOptionPane.ERROR_MESSAGE);
        }
        modelo.addColumn("idProducto");
        modelo.addColumn("Codigo");
        modelo.addColumn("Nombre");
        modelo.addColumn("Precio");
        modelo.addColumn("Fecha");
        modelo.addColumn("Status");
        for(Productos producto : lista){
            modelo.addRow(new Object[]{producto.getIdProducto(),
                                       producto.getCodigo(),
                                       producto.getNombre(),
                                       producto.getPrecio(),
                                       producto.getFecha(),
                                       producto.getStatus()});
        }
        tabla.setModel(modelo);
    }
    
    private void mostrarRegistroTabla(JTable tabla, Productos pro){
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("idProducto");
        modelo.addColumn("Codigo");
        modelo.addColumn("Nombre");
        modelo.addColumn("Precio");
        modelo.addColumn("Fecha");
        modelo.addColumn("Status");
        modelo.addRow(new Object[]{pro.getIdProducto(),
                                   pro.getCodigo(),
                                   pro.getNombre(),
                                   pro.getPrecio(),
                                   pro.getFecha(),
                                   pro.getStatus()});
        tabla.setModel(modelo);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.vista.btnHabilitar.setEnabled(false);
        this.vista.btnDeshabilitar.setEnabled(false);
        if(e.getSource() == this.vista.btnCerrar){
            if(JOptionPane.showConfirmDialog(this.vista, "¿Estas seguro de salir?", this.vista.getTitle(), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                this.vista.setEnabled(false);
                this.vista.dispose();
                System.exit(-1);
            }
        }
        if(e.getSource() == this.vista.btnNuevo){
            this.habilitarComponentes(true);
        }
        if(e.getSource() == this.vista.btnLimpiar){
            this.limpiar();
        }
        if(e.getSource() == this.vista.btnCancelar){
            this.habilitarComponentes(false);
            this.limpiar();
        }
        if(e.getSource() == this.vista.btnGuardar){
            if(!this.isVacio() && this.isValido()){
                try{
                    if(this.db.isExiste(this.vista.txtCodigo.getText(), "0")){
                        Controlador.act = true;
                    }
                    else{
                        Controlador.act = false;
                    }
                }
                catch(Exception ex){
                    JOptionPane.showMessageDialog(this.vista, "Ocurrio un error: " + ex.getMessage(), this.vista.getTitle(), JOptionPane.ERROR_MESSAGE);
                    return;
                }
                this.pro.setCodigo(this.vista.txtCodigo.getText());
                this.pro.setNombre(this.vista.txtNombre.getText());
                this.pro.setPrecio(Float.parseFloat(this.vista.txtPrecio.getText()));
                this.pro.setFecha(String.valueOf(this.vista.jCFecha.getCalendar().get(Calendar.YEAR))+ '-' +
                                  String.valueOf(this.vista.jCFecha.getCalendar().get(Calendar.MONTH))+ '-' +
                                  String.valueOf(this.vista.jCFecha.getCalendar().get(Calendar.DAY_OF_MONTH)));
                if(this.vista.jcbStatus.getSelectedIndex() == 0){
                    this.pro.setStatus(0);
                }
                else{
                    this.pro.setStatus(-1);
                    try{
                        if(this.db.isExiste(this.pro.getCodigo(), "-1")){
                            this.db.actualizar(this.pro);
                            this.mostrarRegistros("-1", this.vista.jtTablaInactivos);
                            JOptionPane.showMessageDialog(this.vista, "Se actualizo un producto con exito", this.vista.getTitle(), JOptionPane.INFORMATION_MESSAGE);
                            return;
                        }
                    }
                    catch(Exception ex){
                        JOptionPane.showMessageDialog(this.vista, "Ocurrio un error al actualizar: " + ex.getMessage(), this.vista.getTitle(), JOptionPane.ERROR_MESSAGE);
                    }
                }
                try{
                    if(!Controlador.act)
                        this.db.insertar(this.pro);
                    else{
                        this.pro.setIdProducto((int)this.vista.jtTabla.getModel().getValueAt(0, 0));
                        this.db.actualizar(this.pro);
                    }
                }
                catch(Exception ex){
                    JOptionPane.showMessageDialog(this.vista, "Ocurrio un error al insertar: " + ex.getMessage(), this.vista.getTitle(), JOptionPane.ERROR_MESSAGE);
                }
                this.vista.jtTabla.setModel(new DefaultTableModel());
                if(!Controlador.act)
                    JOptionPane.showMessageDialog(this.vista, "Se inserto un producto con exito", this.vista.getTitle(), JOptionPane.INFORMATION_MESSAGE);
                else
                    JOptionPane.showMessageDialog(this.vista, "Se actualizo un producto con exito", this.vista.getTitle(), JOptionPane.INFORMATION_MESSAGE);
                Controlador.act = false;
                this.limpiar();
            }
            else{
                JOptionPane.showMessageDialog(this.vista, "Verifica que no haya campos vacios", this.vista.getTitle(), JOptionPane.WARNING_MESSAGE);
            }
        }
        if(e.getSource() == this.vista.btnMostrar){
            this.mostrarRegistros("0", this.vista.jtTabla);
        }
        if(e.getSource() == this.vista.btnBuscarInactivo){
            if(this.vista.txtCodigoInactivo.getText().equals("")){
                JOptionPane.showMessageDialog(this.vista, "No puedes buscar un codigo vacio", this.vista.getTitle(), JOptionPane.WARNING_MESSAGE);
                return;
            }
            Productos pro = null;
            try{
                pro = (Productos)this.db.buscar(this.vista.txtCodigoInactivo.getText(), "-1");
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(this.vista, "Ocurrio un error al buscar: " + ex.getMessage(), this.vista.getTitle(), JOptionPane.ERROR_MESSAGE);
            }
            if(pro != null){
                this.vista.btnHabilitar.setEnabled(true);
            }
        }
        if(e.getSource() == this.vista.btnHabilitar){
            Productos pro = new Productos();
            pro.setCodigo(this.vista.txtCodigoInactivo.getText());
            try{
                this.db.habilitar(pro);
                JOptionPane.showMessageDialog(this.vista, "Se habilito el registro con exito", this.vista.getTitle(), JOptionPane.INFORMATION_MESSAGE);
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(this.vista, "Ocurrio un error al habilitar: " + ex.getMessage(), this.vista.getTitle(), JOptionPane.ERROR_MESSAGE);
            }
        }
        if(e.getSource() == this.vista.btnBuscar){
            if(this.vista.txtCodigo.getText().equals("")){
                JOptionPane.showMessageDialog(this.vista, "No puedes buscar un codigo vacio", this.vista.getTitle(), JOptionPane.WARNING_MESSAGE);
                return;
            }
            Productos pro = null;
            try{
                pro = (Productos)this.db.buscar(this.vista.txtCodigo.getText(), "0");
                this.vista.txtCodigo.setText(pro.getCodigo());
                this.vista.txtNombre.setText(pro.getNombre());
                this.vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                if(pro.getStatus() == -1){
                    this.vista.jcbStatus.setSelectedIndex(1);
                }
                else{
                    this.vista.jcbStatus.setSelectedIndex(0);
                }
                this.vista.jCFecha.setDate(Date.valueOf(pro.getFecha()));
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(this.vista, "Ocurrio un error al listar: " + ex.getMessage(), this.vista.getTitle(), JOptionPane.ERROR_MESSAGE);
            }
            if(pro != null){
                this.vista.btnDeshabilitar.setEnabled(true);
                this.mostrarRegistroTabla(this.vista.jtTabla, pro);
            }
        }
        if(e.getSource() == this.vista.btnDeshabilitar){
            Productos pro = new Productos();
            pro.setCodigo(this.vista.txtCodigo.getText());
            try{
                this.db.deshabilitar(pro);
                JOptionPane.showMessageDialog(this.vista, "Se deshabilito el registro con exito", this.vista.getTitle(), JOptionPane.INFORMATION_MESSAGE);
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(this.vista, "Ocurrio un error al deshabilitar: " + ex.getMessage(), this.vista.getTitle(), JOptionPane.ERROR_MESSAGE);
            }
        }
        this.mostrarRegistros("-1", this.vista.jtTablaInactivos);
        this.mostrarRegistros("0", this.vista.jtTabla);
    }
}
